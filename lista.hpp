#ifndef LISTA_HH
#define LISTA_HH

#include "graf.hpp"

struct Krawedz
{
	int poczatek, koniec, waga;
};

class GrafLista: public Graf{
	Krawedz *krawedz;

public:
	void utworz(const bool czyPetle) const override;
	void wyswietl() const override;
	const int odczytZpliku() override;
	void tworzDane(const int pierwszy) const override;

	const Krawedz * oddaj() const {return krawedz;}
	void dodajKrawedz(int n_pocz, int n_kon, int n_waga, int n) const;
	bool czyJest(int n_pocz, int n_kon) const;

	explicit GrafLista(int w, double gestosc)
		:Graf(w, static_cast<int>(gestosc*w*(w-1)), gestosc),
		krawedz(new Krawedz[static_cast<int>(gestosc*w*(w-1))]) {}
	GrafLista() : Graf() {};
	~GrafLista() {delete[] krawedz;}
};


#endif