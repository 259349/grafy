#ifndef GRAF_HH
#define GRAF_HH

#include <stdlib.h>
#include <iostream>
#include <fstream>

class Graf{
protected: 
	int W, K; //ilość wierzchołków i krawędzi
	double Gestosc; //gęstość grafu

public:
	const int& getW() const {return W;}
	const int& getK() const {return K;}

	virtual void utworz(const bool czyPetle) const=0;				//tworzenie grafu, czyPetle - user decyduje
	virtual void wyswietl() const =0;								//wyświetla graf, do pomocy	
	virtual const int odczytZpliku()=0;	
	virtual void tworzDane(const int poczatek) const=0;

	virtual ~Graf() {};
	explicit Graf(int w, int k, double gestosc) : W(w), K(k), Gestosc(gestosc) {}			//konwersja
	Graf() {};
};

#endif