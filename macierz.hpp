#ifndef MACIERZ_HH
#define MACIERZ_HH

#include "graf.hpp"

#include <cstdlib>
#include <memory>

class GrafMacierz : public Graf {
	std::unique_ptr<std::unique_ptr<int[]>[]> macierz;
public:
	void utworz(const bool czyPetle) const override;
	void wyswietl() const override;
	const int odczytZpliku() override;
	void tworzDane(const int poczatek) const override;

	const int& zwrocWage(int n_wiersz, int n_kolumna) const {return macierz[n_wiersz][n_kolumna];}

	explicit GrafMacierz(int w, double gestosc);
	GrafMacierz() : Graf() {};
	
};


#endif