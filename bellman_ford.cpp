#include "bellman_ford.hpp"

static int  nieskonczonosc = 1000000; 
static int  nieskonczonosc_minus = -1000000;	
									


void rozwiazanie(std::string n_sciezka[], int droga[], int n_W, int pierwszy) {

	std::ofstream plik("Wynik.txt");
	
	std::cout <<"Rozwiązanie\n\n";			
	std::cout << "Wierzchołek startowy: " << pierwszy << "\n\n";

	plik << "Rozwiązanie\n";
	plik << "Wierzchołek startowy: " << pierwszy << "\n\n";

	for (int i = 0; i < n_W; ++i) {
		 
		if (droga[i] == nieskonczonosc_minus) {
			std::cout << i << " koszt=" << "-inf\n"<<";";
			plik << i << " koszt=" << "-inf\n"<<";";
			continue; 
		}																
		else if (droga[i] == nieskonczonosc) {
			std::cout << i << " koszt=" << "inf\n"<<";";										
			plik << i << " koszt=" << "inf\n"<<";";													
			continue; 
		}

		else {
			std::cout << i << " koszt=" << droga[i]<<";";
			plik << i << " koszt=" << droga[i]<<";";
		}

			std::cout << " Ścieżka: " << n_sciezka[i] << i;
			plik << " Ścieżka: " << n_sciezka[i] << i;											
		std::cout << std::endl;
		plik << std::endl;
	}
	std::cout << std::endl;
	plik.close();
}


double bellmanFord(std::shared_ptr<GrafLista> n_graf, int pierwszy, bool pokaz_wynik) {

	std::string* p_sciezka = new std::string[n_graf->getW()]; 

	auto czas_start = std::chrono::high_resolution_clock::now(); 

	int* p_odleglosc = new int[n_graf->getW()];  
	
	for (int i = 0; i < n_graf->getW(); ++i) {

		p_odleglosc[i] = nieskonczonosc;
	}

	p_odleglosc[pierwszy] = 0; 

	for (int i = 1; i < n_graf->getW(); ++i) { 
		for (int j = 0; j < n_graf->getK(); ++j) { 

			int u = n_graf->oddaj()[j].poczatek;
			int v = n_graf->oddaj()[j].koniec;
			int waga = n_graf->oddaj()[j].waga;

			if (p_odleglosc[u] + waga < p_odleglosc[v]) { 
				p_odleglosc[v] = p_odleglosc[u] + waga;
					
				if (pokaz_wynik) { 
						
					p_sciezka[v].clear();
					p_sciezka[v].append(p_sciezka[u] + std::to_string(u) + "->");		//robi kopię
				}
			}
		}
	}


	for (int i = 1; i < n_graf->getW(); ++i) {
		for (int j = 0; j < n_graf->getK(); ++j) {
			
			int u = n_graf->oddaj()[j].poczatek;
			int v = n_graf->oddaj()[j].koniec;
			int waga = n_graf->oddaj()[j].waga;
			if (p_odleglosc[u] + waga < p_odleglosc[v]) {

				if (p_odleglosc[u] > nieskonczonosc/2) p_odleglosc[u] = nieskonczonosc;  
				else p_odleglosc[v] = nieskonczonosc_minus;				
			}
			else if (p_odleglosc[u] > nieskonczonosc/2) p_odleglosc[u] = nieskonczonosc;  
		}
	}
	auto czas_stop = std::chrono::high_resolution_clock::now(); 


	if (pokaz_wynik) rozwiazanie(std::move(p_sciezka), std::move(p_odleglosc), n_graf->getW(), pierwszy);
	delete[] p_odleglosc;
	delete[] p_sciezka;
	return std::chrono::duration<double, std::milli>(czas_stop - czas_start).count(); 
}



double bellmanFord(std::shared_ptr<GrafMacierz> n_graf, int pierwszy, bool pokaz_wynik) {
	
	std::string* p_sciezka = new std::string[n_graf->getW()];

	auto czas_start = std::chrono::high_resolution_clock::now(); 

	int* p_odleglosc = new int[n_graf->getW()];

	for (int i = 0; i < n_graf->getW(); ++i) {

		p_odleglosc[i] = nieskonczonosc;
	}

	p_odleglosc[pierwszy] = 0;

	for (int i = 1; i < n_graf->getW(); ++i) {
		for (int j = 0; j < n_graf->getW(); ++j) {
			for (int w = 0; w < n_graf->getW(); ++w) {

				int u = j;
				int v = w;
				int waga = n_graf->zwrocWage(j, w);
				if (p_odleglosc[j] + waga < p_odleglosc[v]) {
					
					p_odleglosc[v] = p_odleglosc[j] + waga;
					if (pokaz_wynik) {

						p_sciezka[v].clear();
						p_sciezka[v].append(p_sciezka[u] + std::to_string(u) + "->");
					}
				}
			}
		}
	}
	for (int i = 1; i < n_graf->getW(); ++i) {
		for (int j = 0; j < n_graf->getW(); ++j) {
			for (int w = 0; w < n_graf->getW(); ++w) {

				int waga = n_graf->zwrocWage(j, w);
				if (p_odleglosc[j] + waga < p_odleglosc[w]) {
				
					if (p_odleglosc[j] > nieskonczonosc / 2) p_odleglosc[j] = nieskonczonosc;
					else if (waga == nieskonczonosc) continue; 
					else p_odleglosc[w] = nieskonczonosc_minus;
				}
				else if (p_odleglosc[j] > nieskonczonosc/2) p_odleglosc[j] = nieskonczonosc;	
			}
		}
	}
	auto czas_stop = std::chrono::high_resolution_clock::now(); 

	if (pokaz_wynik) rozwiazanie(std::move(p_sciezka), std::move(p_odleglosc), n_graf->getW(), pierwszy);
	delete[] p_odleglosc;
	delete[] p_sciezka;
	return std::chrono::duration<double, std::milli>(czas_stop - czas_start).count(); 
}
