#include "lista.hpp"

static int rownowaga = -1;

void GrafLista::dodajKrawedz(int n_pocz, int n_kon, int n_waga, int n) const {
	krawedz[n].poczatek=n_pocz;
	krawedz[n].koniec=n_kon;
	krawedz[n].waga=n_waga;
}

bool GrafLista::czyJest(int n_pocz, int n_kon) const{
	for (int i=0; i<K; ++i){
		if (krawedz[i].koniec==n_kon && krawedz[i].poczatek==n_pocz) return true;
	}
	return false;
}

void GrafLista::utworz(const bool czyPetle) const {
	if(Gestosc==1){
		int n=0;
		for(int i=0; i<W; ++i){
			for(int j=0; j<W; ++j){
				if(i!=j){
					int waga=rand()%20 + rownowaga;
					while(waga==0){
						waga=rand()%20 + rownowaga;
					}
					dodajKrawedz(i, j, waga, n++);
				}
			}
		}
	}
	else{
		int n_krawedz=0;
		while(n_krawedz<K){
			int p_W=rand()%W;
			int k_W=rand()%W;
			if(!czyJest(p_W, k_W)){
				int n_waga=rand()%20 + rownowaga;
				while(n_waga==0){
					n_waga=rand()%20 + rownowaga;
				}

				if(p_W!=k_W)dodajKrawedz(p_W, k_W, n_waga, n_krawedz++);
				else if(czyPetle) dodajKrawedz(p_W, k_W, n_waga, n_krawedz++);
			}
		}
	}
}

void  GrafLista::wyswietl() const{
	std::cout<<"Graf reprezentowany przez listę sąsiedztwa"<<std::endl;
	for(int i=0; i<W; ++i){
		std::cout<<i<<" : ";
		for(int j=0; j<K; ++j){
			if(krawedz[j].poczatek==i)
				std::cout<<"["<<krawedz[j].koniec <<"|"<<krawedz[j].waga<<"] ";
		}
		std::cout<<"\n";
	}
	std::cout<<std::endl;
}

const int GrafLista::odczytZpliku(){
	std::ifstream plik("dane.txt");
	if (!plik.is_open()) {
		std::cout<< "Plik nie został otwarty."<<std::endl;
		return 1;
	}
	int pierwszy, poczatek, koniec, waga;
	plik >> K >> W >> pierwszy;
	krawedz = new Krawedz[K];


	for (int i = 0; i < K; ++i) {

		plik >> poczatek >> koniec >> waga;
		dodajKrawedz(poczatek, koniec, waga, i);
	}
	plik.close();
	return pierwszy; 
}


void GrafLista::tworzDane(const int pierwszy) const {

	std::ofstream plik("NoweDane.txt");
	if (!plik.is_open()) {
		std::cout<<  "Nie można otworzyć pliku"<<std::endl;
		return;
	}

	plik << K << " " << W << " " << pierwszy << "\n";
	for (int i = 0; i < K; ++i) {

		plik << krawedz[i].poczatek << " ";
		plik << krawedz[i].koniec << " ";
		plik << krawedz[i].waga << "\n";		
	}
	plik.close();
}
