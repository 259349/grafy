#ifndef BELLMAN_FORD_HH
#define BELLMAN_FORD_HH

#include <chrono>
#include <string> 
#include <memory>

#include "lista.hpp"
#include "macierz.hpp"


double bellmanFord(std::shared_ptr<GrafLista> n_graf, int pierwszy, bool rozwiazanie);

double bellmanFord(std::shared_ptr<GrafMacierz> n_graf, int pierwszy, bool rozwiazanie);

#endif