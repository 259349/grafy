#include "macierz.hpp"
#include <cstdlib>
#include <memory>

static int nieskonczonosc = 1000000; 

static int rownowaga = -1; 
			

GrafMacierz::GrafMacierz(int W, double gestosc) 
	: Graf(W, static_cast<int>(gestosc* W* (W - 1)), gestosc), 
	  macierz(std::make_unique<std::unique_ptr<int[]>[]>(W)) {

	
	for (int i = 0; i < W; ++i) {

		macierz[i] = std::make_unique<int[]>(W); 
		for (int j = 0; j < W; ++j) {
			
			if (i == j) macierz[i][j] = 0; 
			else macierz[i][j] = nieskonczonosc;
		}
	}
}


void GrafMacierz::utworz(const bool czyPetle) const {

	if (Gestosc == 1) {  
		for (int i = 0; i < W; ++i) { 
			for (int j = 0; j < W; ++j) {  
				if (i != j) 
				{
					int n_waga = rand() % 20  + rownowaga;
					while (n_waga == 0) { 

						n_waga = rand() % 20 + rownowaga;
					}
					macierz[i][j] = n_waga;
				}
			}
		}
	}
	else { 

		int n_krawedz = K;

		while (n_krawedz) { 

			int n_wiersz = rand() % W; 
			int n_kolumna = rand() % W;

			
			if (macierz[n_wiersz][n_kolumna] == 0|| macierz[n_wiersz][n_kolumna]== nieskonczonosc) { 

				int n_waga = rand() % 20 + rownowaga;
				while (n_waga == 0) { 

					n_waga = rand() % 20 + rownowaga;
				}

				
				if (n_wiersz != n_kolumna) {

					macierz[n_wiersz][n_kolumna] = n_waga;
					--n_krawedz;
				}
				else if (czyPetle) {

					macierz[n_wiersz][n_kolumna] = n_waga;
					--n_krawedz;
				}
			}
		}
	}
}


void GrafMacierz::wyswietl() const {

	std::cout<<"Graf reprezentowany przez macierz sąsiedztwa"<<std::endl;
	for (int i = 0; i < W; ++i) {
		
		if (i <= 10) std::cout << "    " << i;
		else if (i <= 100) std::cout << "   " << i;
		else std::cout << "  " << i;
	}
	std::cout << "\n\n";


	for (int i = 0; i < W; ++i) {	
		
		if (i < 10) std::cout << i << "   |";
		else if (i < 100) std::cout << i << "  |";
		else std::cout << i << " |";
		
		for (int j = 0; j < W; ++j) {

			int n = macierz[i][j];

			if (n == nieskonczonosc) std::cout << "*"; 
			else std::cout << n;					

			if (abs(n) < 10 || abs(n) == nieskonczonosc) std::cout << "    "; 
			else if (abs(n) < 100) std::cout << "   ";
			else std::cout << "  ";

			if (n < 0) std::cout << '\b';
		}
		std::cout << "|\n";
	}
	std::cout << std::endl;
}



const int GrafMacierz::odczytZpliku() {

	std::ifstream plik("dane.txt"); 
	if (!plik.is_open()) {
		std::cout<<"Nie można otworzyć pliku z danymi."<<std::endl;
		return 1;
	}

	
	int pierwszy, poczatek, koniec, waga;
	plik >> K >> W >> pierwszy;
	macierz = std::make_unique<std::unique_ptr<int[]>[]>(W);


	for (int i = 0; i < W; ++i) {

		macierz[i] = std::move(std::make_unique<int[]>(W));
		for (int j = 0; j < W; ++j) {

			if (i == j) macierz[i][j] = 0;
			else macierz[i][j] = nieskonczonosc;
		}
	}

	
	for (int j = 0; j < K; ++j) {

		plik >> poczatek >> koniec >> waga;
		macierz[poczatek][koniec] = waga;;
	}
	plik.close();
	return pierwszy;
}


void GrafMacierz::tworzDane(const int poczatek) const {
	
	std::ofstream plik("NoweDane.txt");
	if (!plik.is_open()) {
		std::cout<<  "Nie można otworzyć pliku"<<std::endl;
		
		return;
	}

	plik << K << " " << W << " " << poczatek << "\n";
	for (int i = 0; i < W; ++i) {
		for (int j = 0; j < W; ++j) {

			if (macierz[i][j] != nieskonczonosc && macierz[i][j] != 0) {

				plik << i << " ";
				plik << j << " ";
				plik << macierz[i][j] << "\n";
			}
		}
	}
	plik.close();
}
