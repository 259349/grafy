#include <time.h>

#include "bellman_ford.hpp"


template<typename T>
std::ostream& testy(std::ostream& plik, int (&ilosc_w)[5], double (&ilosc_g)[4], int n_petli) {
	
	for (double gestosc : ilosc_g) { 
		for (int wierzcholki : ilosc_w) { 
			double wynik = 0;
			for (int i = 0; i < n_petli; ++i) { 

				std::shared_ptr<T> graf = std::make_shared<T>(wierzcholki, gestosc); 
				int start = rand() % wierzcholki; 

				graf->utworz(true); 

				wynik += bellmanFord(std::move(graf), start, false); 
				
			}
			plik << wynik / n_petli << " "; 
			std::cout << wierzcholki << " wierzchołków - czas: "<<wynik/n_petli << std::endl;
		}
		std::cout << "\n" << gestosc << " gęstość\n" << std::endl;
	}
	plik << "\n";
	return plik;
}


int main() {

	srand(time(NULL));

	bool test_pierwszy = true; 



	if (test_pierwszy) {
		
		int ilosc_w[5] = {5, 15, 30, 50, 100}; 
		double ilosc_g[4] = {0.25, 0.5, 0.75, 1}; 
		int n_petli = 100;		
		

	std::ofstream plik("PomiarCzasu.txt");
		if (!plik.is_open()) {

			std::cout << "Plik do zapisu wyników nie otwarty," << std::endl;
			return 1;
		}

		testy<GrafLista>(plik, ilosc_w, ilosc_g, n_petli); 
		std::cout << "Reprezentacja grafu w postaci listy sąsiedztwa zakończona.\n" << std::endl;
		testy<GrafMacierz>(plik, ilosc_w, ilosc_g, n_petli);	
		std::cout << "Reprezentacja grafu w postaci macierzy sąsiedztwa zakończona." << std::endl;

		plik.close();
		return 0;
	}


	
	typedef	GrafLista graf_test; 
	bool jest_plik = false;	
	bool czyPetla = true;	
	int wierzcholki = 10;		
	double gestosc = 1;	
	int start = 5;		


	std::shared_ptr<graf_test> n_graf;

	if (jest_plik) { 
		std::shared_ptr<graf_test> tmp = std::make_shared<graf_test>();
		n_graf = tmp;
			start = tmp->odczytZpliku(); 
	}
	else { 
		std::shared_ptr<graf_test> tmp = std::make_shared<graf_test>(wierzcholki, gestosc);
		n_graf = tmp;
		tmp->utworz(czyPetla); 
	}

	n_graf->wyswietl(); 

		if (!jest_plik) n_graf->tworzDane(start); 
	
	bellmanFord(std::move(n_graf), start, true); 

	return 0;
}